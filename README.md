# Custom Test Cases for AUD

## GrayCode Custom Tests
````java
@Test(timeout = 666)
public void customTestLength0() {
	BigBrother gcc = new BigBrother();
	String[] actual = GrayCode.generate(gcc, 0);
	System.out.println(actual.length);
	assertEquals("Unexpected length of returned Gray code array, should 0", 0, actual.length);
}
````

## Skyline Custom Test
### Area Custom Tests
````java
@Test(timeout = 666)
public void customTestAreaLowSkyline() {
	int[] skyline = { 1, 0, 3, 0, 9, 0, 12, 0, 16, 0, 19, 0, 22, 0, 23, 0, 29, 0 };
	int[] skylineClone = Arrays.copyOf(skyline, skyline.length);
	int expected = 0;
	int actual = SkylineSolver.area(skylineClone);
	//System.out.println(Arrays.toString(skyline)+" area: "+actual);
	assertEquals(SkylineSolverPublicTest.METHOD_NAME_area + "(" + Arrays.toString(skyline) + ") failed", expected, actual);
	assertArrayEquals(SkylineSolverPublicTest.METHOD_NAME_area + " - DO NOT MODIFY THE INPUT!", skyline, skylineClone);
}
````
### Divide Custom Tests
````java
@Test(timeout = 666)
public void customTest_divideZero() {
	int[][] buildings = {{}};
	int[][] buildingsClone = {{}};
	int[][] nullArray = new int[0][0];
	int[][] actual = SkylineSolver.divide(buildingsClone, 0, true);
	assertNotNull(SkylineSolverPublicTest.METHOD_NAME_divide + "(" + Arrays.deepToString(actual) + ", 0, true) must not return null", actual);
	assertArrayEquals("divide by zero failed array:"+ Arrays.deepToString(actual)+" differs from: "+Arrays.deepToString(nullArray), nullArray, actual);
	assertArrayEquals(SkylineSolverPublicTest.METHOD_NAME_divide + " - DO NOT MODIFY THE INPUT!", buildings, buildingsClone);
}
@Test(timeout = 666)
public void customTest__divide__2_of_many__true() {
	int[][] buildings = { { 1, 11, 5 }, { 2, 6, 7 }, { 3, 13, 19 }, {4, 15, 8} };
	int[][] buildingsClone = { { 1, 11, 5 }, { 2, 6, 7 }, { 3, 13, 19 }, {4, 15, 8} };
	int[][] actual = SkylineSolver.divide(buildingsClone, 2, true);
	assertNotNull(SkylineSolverPublicTest.METHOD_NAME_divide + "(" + Arrays.deepToString(buildings) + ", 2, true) must not return null", actual);
	assertArrayEquals(SkylineSolverPublicTest.METHOD_NAME_divide + "(" + Arrays.deepToString(buildings) + ", 2, true) failed", new int[][] { { 1, 11, 5 }, { 2, 6, 7 } }, actual);
	assertArrayEquals(SkylineSolverPublicTest.METHOD_NAME_divide + " - DO NOT MODIFY THE INPUT!", buildings, buildingsClone);
}

@Test(timeout = 666)
public void customTest__divide__3_of_many__true() {
	int[][] buildings = { { 1, 11, 5 }, { 2, 6, 7 }, { 3, 13, 19 }, {4, 15, 8} };
	int[][] buildingsClone = { { 1, 11, 5 }, { 2, 6, 7 }, { 3, 13, 19 }, {4, 15, 8} };
	int[][] actual = SkylineSolver.divide(buildingsClone, 3, true);
	assertNotNull(SkylineSolverPublicTest.METHOD_NAME_divide + "(" + Arrays.deepToString(buildings) + ", 3, true) must not return null", actual);
	assertArrayEquals(SkylineSolverPublicTest.METHOD_NAME_divide + "(" + Arrays.deepToString(buildings) + ", 3, true) failed", new int[][] { { 1, 11, 5 }, { 2, 6, 7 }, { 3, 13, 19 } }, actual);
	assertArrayEquals(SkylineSolverPublicTest.METHOD_NAME_divide + " - DO NOT MODIFY THE INPUT!", buildings, buildingsClone);
}

@Test(timeout = 666)
public void customTest__divide__2_of_many__false() {
	int[][] buildings = { { 1, 11, 5 }, { 2, 6, 7 }, { 3, 13, 19 }, {4, 15, 8} };
	int[][] buildingsClone = { { 1, 11, 5 }, { 2, 6, 7 }, { 3, 13, 19 }, {4, 15, 8} };
	int[][] actual = SkylineSolver.divide(buildingsClone, 2, false);
	assertNotNull(SkylineSolverPublicTest.METHOD_NAME_divide + "(" + Arrays.deepToString(buildings) + ", 2, true) must not return null", actual);
	assertArrayEquals(SkylineSolverPublicTest.METHOD_NAME_divide + "(" + Arrays.deepToString(buildings) + ", 2, true) failed", new int[][] { { 3, 13, 19 }, {4, 15, 8} }, actual);
	assertArrayEquals(SkylineSolverPublicTest.METHOD_NAME_divide + " - DO NOT MODIFY THE INPUT!", buildings, buildingsClone);
}

@Test(timeout = 666)
public void customTest__divide__3_of_many__false() {
	int[][] buildings = { { 1, 11, 5 }, { 2, 6, 7 }, { 3, 13, 19 }, {4, 15, 8} };
	int[][] buildingsClone = { { 1, 11, 5 }, { 2, 6, 7 }, { 3, 13, 19 }, {4, 15, 8} };
	int[][] actual = SkylineSolver.divide(buildingsClone, 1, false);
	assertNotNull(SkylineSolverPublicTest.METHOD_NAME_divide + "(" + Arrays.deepToString(buildings) + ", 1, true) must not return null", actual);
	assertArrayEquals(SkylineSolverPublicTest.METHOD_NAME_divide + "(" + Arrays.deepToString(buildings) + ", 1, true) failed", new int[][] { { 2, 6, 7 }, { 3, 13, 19 }, {4, 15, 8} }, actual);
	assertArrayEquals(SkylineSolverPublicTest.METHOD_NAME_divide + " - DO NOT MODIFY THE INPUT!", buildings, buildingsClone);
}
````
### Conquer Custom Tests
````java
@Test(timeout = 666)
public void customTest_conquerLowSkyline() {
	SkylineSolverPublicTest.BigBrother ssh = new SkylineSolverPublicTest.BigBrother();
	
	int[][] buildings = new int[][] { { 2, 0, 7 }, { 3, 0, 9 }, { 12, 0, 16 }, { 14, 0, 25 }, { 19, 0, 22 }, { 23, 0, 29 }, { 24, 0, 28 } };
	int[] expected = { 2, 0 };
	int[] actual = SkylineSolver.conquer(ssh, buildings);
	//System.out.println("result: "+Arrays.toString(actual)+" expected: "+Arrays.toString(expected));
	assertArrayEquals(SkylineSolverPublicTest.METHOD_NAME_area + " - DO NOT MODIFY THE INPUT!", actual, expected);
}
````
````java
@Test(timeout = 666)
public void customTest_conquerUnevenNumberOfBuildings(){
	SkylineSolverPublicTest.BigBrother ssh = new SkylineSolverPublicTest.BigBrother();
	
	int[][] buildings = { { 1, 11, 5 }, { 2, 6, 7 }, { 3, 13, 9 }};
	int[] expected = {1 , 11 , 3 , 13 , 9 , 0};
	int[] actual = SkylineSolver.conquer(ssh, buildings);
	assertArrayEquals("Wrong output at customTest_conquerUnevenNumberOfBuildings()",expected , actual);
}
````